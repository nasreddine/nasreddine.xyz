var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var minifyJs = require('gulp-uglify');
var rename = require('gulp-rename');
var watch = require('gulp-watch');

gulp.task('default', function() {

    gulp.src(['./css/*.css'])
        .pipe(watch('./css/*.css'))
        .pipe(minifyCss())
        .pipe(rename("webstyle.css"))
        .pipe(gulp.dest('./prod/'));

    gulp.src('./js/*.js')
        .pipe(watch('./js/*.js'))
        .pipe(minifyJs())
        .pipe(rename("webjs.js"))
        .pipe(gulp.dest('./prod/'));
});